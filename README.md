# Ansible Playbooks for Azure

This repository contains steps for launching a VM on Azure via Cloudforms and Ansible Tower.

## Prerequisites

- Azure Account. If you don't have it, get a [free one](https://azure.microsoft.com/en-us/free/).

  To authenticate with Azure, generate [service principal](https://docs.microsoft.com/en-us/azure/azure-resource-manager/resource-group-create-service-principal-portal) and expose them as environment variables or store them as a file.
  
  Nice to have authentication [Azure environment variables](https://docs.ansible.com/ansible/2.6/scenario_guides/guide_azure.html#using-environment-variables)

- Install Cloudforms 4.6 and Ansible Tower 3.2.5
- Install Azure dependencies pacakage on Ansible Tower venv
  ```
  source /var/lib/awx/venv/awx/bin/activate
  pip install ansible[azure]
  deactivate
  source /var/lib/awx/venv/ansible/bin/activate
  pip install ansible[azure]
  deactivate
  pip install ansible[azure]
  ```
- Login to Ansible Tower
- Go to credentials and add the Azure credentials and SSH credentials to be used to login to Azure provisioned instances
- Go to inventory and add the Azure inventory source using the Azure credentials. Mark update on launch
- Go to projects and add the following git repo https://gitlab.com/redhat-cop/EMEACloudFormsHackathon/ansible-azure.git and select the development branch
- Sync the repository
- Create a new job template using the localhost inventory, Both the Azure and SSH credentials, Use the VMCreate.yml playbook. Add ```vm_name``` and ```resource_group``` in the extra_vars section and check prompt on launch
- Login to Cloudforms
- Add Ansible Tower provider
- Go to Tower Explorer
- Right click the template created earlier and click create service dialog
- Edit service dialog to make fields not read only
- Create catalog item as tower template and use the created service dialog
- Launch the newly created service
- Sitback and watch VM provisioned on Azure :)
